<?php

namespace cursophp7\app\exceptions;

use Exception;

class FileException extends Exception
{
    public function __construct(string $message)
    {
        parent::__construct($message);
    }
}