<?php

use cursophp7\app\exceptions\AppException;
use cursophp7\app\exceptions\NotFoundExeption;
use cursophp7\core\App;
use cursophp7\core\Request;

try
{
    require __DIR__ . '/../core/bootstrap.php';

    App::get('router')->direct(implode([Request::uri()]),Request::method());
}
catch (NotFoundExeption $notFoundException)
{
    die($notFoundException->getMessage());
}
catch (AppException $appException)
{
    die($appException->getMessage());
}