<?php

namespace cursophp7\app\controllers;

use cursophp7\app\entity\Asociado;
use cursophp7\app\entity\Mensaje;
use cursophp7\app\exceptions\AppException;
use cursophp7\app\exceptions\FileException;
use cursophp7\app\exceptions\QueryException;
use cursophp7\app\exceptions\ValidationException;
use cursophp7\app\repository\MensajeRepository;
use cursophp7\app\utils\File;
use cursophp7\core\App;
use cursophp7\core\Response;
class MensajeController
{
    /**
     * @throws QueryException
     */
    public function index()
    {
        Response::renderView('contact');
    }

    /**
     * @throws AppException
     * @throws QueryException
     */
    public function nuevo()
    {
        try{

            $nombre = trim(htmlspecialchars($_POST['nombre']));
            $apellidos = trim(htmlspecialchars($_POST['apellidos']));
            $email = trim(htmlspecialchars($_POST['email']));
            $asunto = trim(htmlspecialchars($_POST['asunto']));
            $texto = trim(htmlspecialchars($_POST['texto']));

            if(empty($nombre))
                throw new ValidationException('El nombre no puede quedar vacío');

            if(empty($email))
                throw new ValidationException('El e-mail no puede quedar vacío');
            else
            {
                if(filter_var($email, FILTER_VALIDATE_EMAIL)===false)
                    throw new ValidationException('El e-mail no es válido');
            }

            if(empty($asunto))
                throw new ValidationException('El asunto no puede quedar vacío');

            if(empty($errores))
            {
                $mensaje = new Mensaje($nombre, $apellidos, $asunto, $email, $texto);

                App::getRepository(MensajeRepository::class)->save($mensaje);

                $message = "Se ha guardado una nuevo mensaje: " . $mensaje->getTexto();
                App::get('logger')->add($message);
            }

        }
        catch (ValidationException $validationException)
        {
            die($validationException->getMessage());
        }

        App::get('router')->redirect('contact');
    }

}